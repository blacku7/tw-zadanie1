import React, { useState, useEffect } from "react";
import {
  BsFillHandThumbsUpFill,
  BsFillHandThumbsDownFill,
} from "react-icons/bs";
import "./App.css";

function App() {
  // Krok 1 - Inicjalizacja zmiennych
  const [todos, setTodos] = useState([]);
  const [task, setTask] = useState("");
  const [completed, setCompleted] = useState([]);
  const [editIndex, setEditIndex] = useState(null); // Indeks zadania do edycji
  const [errorMessage, setErrorMessage] = useState(""); // Komunikat o błędzie

  // Funkcja do wczytywania zadań z localStorage
  useEffect(() => {
    const storedTodos = localStorage.getItem("todos");
    const storedCompleted = localStorage.getItem("completed");
    if (storedTodos) {
      setTodos(JSON.parse(storedTodos));
    }

    if (storedCompleted) {
      setCompleted(JSON.parse(storedCompleted));
    }
  }, []);

  // Krok 2 - Funkcja do dodawania/edycji zadania
  const addTask = () => {
    if (task.trim() !== "") {
      const newTodos = [...todos];
      const newCompleted = [...completed];

      if (editIndex !== null) {
        newTodos[editIndex] = task; // Jeśli edytujemy, aktualizujemy istniejące zadanie
      } else {
        newTodos.push(task); // Jeśli dodajemy, dodajemy nowe zadanie
        newCompleted.push(false);
      }

      setTodos(newTodos);
      setTask("");
      setCompleted(newCompleted);
      localStorage.setItem("todos", JSON.stringify(newTodos));
      localStorage.setItem("completed", JSON.stringify(newCompleted));
      setErrorMessage(""); // Czyści komunikat o błędzie po dodaniu/edycji zadania
      setEditIndex(null); // Resetuje indeks zadania do edycji
    } else {
      setErrorMessage("Wprowadź nazwę zadania!"); // Ustawia komunikat o błędzie
    }
  };

  // Krok 3 - Funkcja do usuwania zadania na podstawie indeksu
  const deleteTodo = (index) => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
    localStorage.setItem("todos", JSON.stringify(newTodos));
  };

  // Funkcja do zmiany statusu zadania
  const toggleCompleted = (index) => {
    const newCompleted = [...completed];
    newCompleted[index] = !newCompleted[index];
    setCompleted(newCompleted);
    localStorage.setItem("completed", JSON.stringify(newCompleted));
  };

  // Funkcja do przełączania edycji
  const toggleEdit = (index) => {
    setEditIndex((prevIndex) => (prevIndex === index ? null : index));
    setTask(editIndex === index ? "" : todos[index]); // Resetuje zadanie do edycji
  };

  return (
    <div className="container text-center mt-5">
      <h1>Lista zadań</h1>

      <input
        type="text"
        value={task}
        onChange={(e) => setTask(e.target.value)}
        placeholder="Dodaj zadanie"
      />
      <br></br>
      <button
        type="button"
        className="btn btn-success btn-sm mt-2"
        onClick={addTask}
      >
        {editIndex !== null ? "Edytuj" : "Dodaj"}
      </button>

      {/* Komunikat o błędzie */}
      {errorMessage && (
        <div className="alert alert-warning mt-2" role="alert">
          {errorMessage}
        </div>
      )}

      {/* Tabela z zadaniami */}
      <table className="table mt-3">
        <thead>
          <tr>
            <th>Nazwa zadania</th>
            <th>Dostępne akcje</th>
            <th>Status zadania</th>
          </tr>
        </thead>
        <tbody>
          {todos.map((todo, index) => (
            <tr key={index}>
              <td>
                {/* Warunek dla pola edycji */}
                {editIndex === index ? (
                  <input
                    type="text"
                    value={task}
                    onChange={(e) => setTask(e.target.value)}
                  />
                ) : (
                  todo
                )}
              </td>
              <td>
                {/* Przycisk usuwania */}
                <button
                  type="button"
                  className="btn btn-danger btn-sm"
                  onClick={() => deleteTodo(index)}
                >
                  Usuń
                </button>
                {/* Przycisk edycji */}
                <button
                  type="button"
                  className="btn btn-warning btn-sm ml-2"
                  onClick={() => toggleEdit(index)}
                >
                  Edytuj
                </button>
              </td>
              {/* Przycisk zmiany statusu */}
              <td>
                <button
                  className={`btn ${
                    completed[index]
                      ? "btn-success btn-sm"
                      : "btn-danger btn-sm"
                  }`}
                  onClick={() => toggleCompleted(index)}
                >
                  {completed[index] ? (
                    <BsFillHandThumbsUpFill />
                  ) : (
                    <BsFillHandThumbsDownFill />
                  )}
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default App;
